<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/read/filter.php';
require_once FONGO_DIR . '/read/rule.php';
require_once FONGO_DIR . '/struct/result.php';
require_once FONGO_DIR . '/process/garbage.php';
require_once FONGO_DIR . '/chain/filter.php';
require_once FONGO_DIR . '/chain/validate.php';

function fongo_process(array $dict, array $input, array $context = []): fongo_result
{

  $result = new fongo_result();

  list($result->processed, $result->garbage) = fongo_process_garbage($dict, $input);

  /* composed list of fields ordered by processing */
  $ordered = [];
  $after = [];
  foreach ($dict as $field => $term) {
    if ('' !== $term->compose) {
      $after[$field] = true;
    } else {
      $ordered[$field] = true;
    }
  }
  $ordered += $after;

  /* process ordered list */
  foreach (array_keys($ordered) as $field) {
    $term = $dict[$field];
    $field_missed = !array_key_exists($field, $result->processed);

    /* compose term */
    if ('' !== $term->compose) {
      $result->processed[$field] = $dict[$field]->compose;
      $fields = [];
      preg_match_all('/{+(.*?)}/', $term->compose, $fields);
      foreach ($fields[1] ?? [] as $c_field) {
        $result->processed[$field] = str_replace(
          '{' . $c_field . '}',
          (array_key_exists($c_field, $result->processed) ? (string) $result->processed[$c_field] : ''),
          $result->processed[$field]);
      }
      $field_missed = false;
    }

    /* fill if empty */
    $filled = false;
    switch ($dict[$field]->fill_source) {
      case 'input':
        if (array_key_exists($dict[$field]->fill_value, $input)) {
          $value_to_fill = $input[$dict[$field]->fill_value];
          $filled = true;
        }
        break;
      case 'context':
        if (array_key_exists($dict[$field]->fill_value, $context)) {
          $value_to_fill = $context[$dict[$field]->fill_value];
          $filled = true;
        }
        break;
      case 'default':
        $value_to_fill = $dict[$field]->dafault;
        $filled = true;
        break;
      case 'value':
        $value_to_fill = $dict[$field]->fill_value;
        $filled = true;
        break;
    }
    if ($filled) {
      $result->processed[$field] = $value_to_fill;
      $field_missed = false;
    }

    /* missed management */
    if ($field_missed && $term->required) {
      $result->missed[$field] = true;
      $result->errors[$field] = $term->missing_message;
    } else if (!$field_missed) {
      /* check if empty_allowed and valid empty_function function */
      if ($term->empty_allowed && is_callable($term->empty_function['function'] ?? '')) {
        $field_is_empty = $term->empty_function['function'](
          $result->processed[$field],
          $term->empty_function['args'] ?? []);
      } else {
        $field_is_empty = false;
      }
      if ($field_is_empty) {
        $result->empties[$field] = true;
      } else {
        /* filter field */
        if (!empty($term->filters)) {
          list($value_filtered, $error) = fongo_chain_filter(
            fongo_read_filter($term->filters), //filters
            $result->processed[$field] // value
          );
          if ('' === $error) {
            $result->processed[$field] = $value_filtered;
            $result->filtered[$field] = true;
          }
        }

        /* validation */
        if (!empty($dict[$field]->rules)) {
          list($message, $code) = fongo_chain_validate(
            fongo_read_rule($dict[$field]->rules), // rules
            $result->processed[$field], // value,
            array_merge(['input' => $input], $context) // context
          );
          $result->validated[$field] = true;
          if ('' !== $message) {
            $result->errors[$field] = $message;
            $result->codes[$field] = $code;
          }
        }
      }
    }
  }

  return $result;
}
