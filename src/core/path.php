<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function fongo_path(string $name = null, string $dir = null)
{
  static $cache = [];
  if (null === $name && null === $dir) {
    /* all */
    return $cache;
  } elseif (null !== $dir) {
    /* set */
    $cache[$name] = $dir;
    return null;
  } else {
    /* get */
    return $cache[$name] ?? '';
  }
}
