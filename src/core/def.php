<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function fongo_def(array $def): array
{
  $error = '';
  if (isset($def['require'])) {
    if (!is_file($def['require']) || !is_readable($def['require'])) {
      $error =  sprintf('File %s not readable', $def['require']);
    } else {
      require_once $def['require'];
    }
  } elseif (!isset($def['function'])) {
    $error = 'function not defined';
  } elseif (!is_callable($def['function'])) {
    $error =  sprintf('Function %s not callable', $def['function']);
  }
  if ('' === $error) {
    return [$def['function'], $def['args'] ?? [], $error];
  } else {
    return ['', [], $error];
  }
}
