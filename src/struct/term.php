<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

final class fongo_term
{

  public string $type = 'var';
  public string $compose = '';
  public string|null|int|float|bool $default = null;
  public bool $required = true;
  public bool $empty_allowed = false;
  public array $empty_function;
  public string $missing_message = 'Missing field';
  public string $fill_source = '';
  public string|null|int|float|bool $fill_value = null;
  public array $filters = [];
  public array $rules = [];
  public string $option = '';

  public function __get(string $name): mixed
  {
    throw new exception("($name) __get not allowed on fongo_term");
  }

  public function __set($name, $value)
  {
    throw new exception('__set not allowed on fongo_term');
  }

  public function __construct(array $config = [])
  {
    foreach ($config as $key => $value) {
      if (isset($this->$key) || 'default' === $key || 'fill_value' === $key) {
        $this->$key = $value;
      }
    }
  }
}
