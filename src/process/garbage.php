<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function fongo_process_garbage(array $dict, array $input): array
{
  $cleaned = [];
  $garbage = [];
  foreach ($input as $field => $value) {
    if (!isset($dict[$field])) {
      $garbage[$field] = $value;
    } else {
      $cleaned[$field] = $value;
    }
  }
  return [$cleaned, $garbage];
}
