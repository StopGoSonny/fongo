<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

const FONGO_DIR = __DIR__;
const FONGO_VERSION = '2024.1';
const FONGO_CODENAME = 'Staghorn Coral (Acropora cervicornis)';
