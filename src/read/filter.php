<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/read/config.php';
require_once FONGO_DIR . '/core/compile.php';

function fongo_read_filter(array $list): array
{
  static $cache = [];
  $result = [];
  foreach ($list as $name) {
    if (!isset($cache[$name])) {
      $cache[$name] = fongo_read_config(fongo_compile($name));
    }
    $result[$name] = $cache[$name];
  }
  return $result;
}
