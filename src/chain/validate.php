<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/core/def.php';

function fongo_chain_validate(array $rules, $value, array $context = []): array
{
  $message = '';
  $code = '';
  $config_ok = true;
  foreach ($rules ?? [] as $index => $rule) {
    list($validate_func, $validate_args, $error) = fongo_def($rule);
    if ('' === $error) {
      $args = $validate_args;
      foreach($args as $key => $arg) {
        if (is_string($arg) && false !== strpos($arg, 'context')) {
          $parts = explode('::', $arg);
          if (isset($context[$parts[1]])) {
            $args[$key] = $context[$parts[1]] ?? null;
          } else {
            /* invalid context field */
            $config_ok = false;
            $rule['message'] = 'Invalid context';
            break;
          }
        }
      }
    } else {
      $rule['message'] = $error;
      $config_ok = false;
    }
    $is_valid = $config_ok ? $validate_func($value, ...$args) : false;
    if (!$is_valid) {
      $message = $rule['message'] ?? 'Invalid value';
      $code = $rule['code'] ?? $index;
      break;
    }
  }
  return [$message, $code];
}
