<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/core/def.php';

function fongo_chain_filter(array $filters, $value): array
{
  $filtered = $value;
  $error = '';
  foreach ($filters as $filter) {
    list($filter_func, $filter_args, $error) = fongo_def($filter);
    if ('' === $error) {
      $filtered = $filter_func($value, ...$filter_args);
    } else {
      break;
    }
  }
  return [$filtered, $error];
}
