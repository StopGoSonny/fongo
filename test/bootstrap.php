<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$root_dir = realpath(__DIR__ . '/..');

require $root_dir . '/src/initme.php';

define('FONGO_TEST_DIR', $root_dir . '/test');

const FONGO_TEST_DATA_DIR = FONGO_TEST_DIR . '/data';
const FONGO_TEST_FUNC_DIR = FONGO_TEST_DIR . '/func';
