<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

function fongo_test_rule_greater_than($value, $min, bool $equal = false): bool
{
  if ($equal) {
    return $value >= $min;
  } else {
    return $value > $min;
  }
}
