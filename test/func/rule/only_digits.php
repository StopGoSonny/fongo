<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

function fongo_test_rule_only_digits(string $value): bool
{
  return 0 === preg_match('/[^0-9]/', $value);
}
