<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

function fongo_test_rule_is_valid_date(string $value): bool
{
  return false !== date_create_from_format('Y-m-d', $value);
}
