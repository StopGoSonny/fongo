<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

function fongo_test_rule_is_email(string $value): bool
{
  return false !== strpos($value, '@');
}
