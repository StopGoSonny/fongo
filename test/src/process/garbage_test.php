<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/struct/term.php';
    require_once FONGO_DIR . '/process/garbage.php';
  }


  public function t_one_valid_field(test $t)
  {
    $t->wie = [
      ['valid_field' => 'value'], // cleaned
      ['garbage' => 'value'] // garbage
    ];
    $t->wig = fongo_process_garbage(
      ['valid_field' => new fongo_term([])], // dict
      ['valid_field' => 'value', 'garbage' => 'value'] //input
    );
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_no_valid_field(test $t)
  {
    $t->wie = [
      [], // cleaned
      ['garbage' => 'value', 'another_garbage' => 'value'] // garbage
    ];
    $t->wig = fongo_process_garbage(
      ['valid_field' => new fongo_term([])], // dict
      ['garbage' => 'value', 'another_garbage' => 'value'] //input
    );
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_no_garbage(test $t)
  {
    $t->wie = [
      ['valid_field' => 'value'], // cleaned
      [] // garbage
    ];
    $t->wig = fongo_process_garbage(
      ['valid_field' => new fongo_term([])], // dict
      ['valid_field' => 'value'] //input
    );
    $t->pass_if($t->wie === $t->wig);
  }

}
