<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/core/process.php';
    require_once FONGO_DIR . '/core/path.php';
    require_once FONGO_DIR . '/read/dict.php';
    fongo_path('data', FONGO_TEST_DATA_DIR);

    $this->context = [
      'fill_field' => ' 100 ',
      'field_with_only_filter' => ' andrea ',
      'field_with_only_rule' => 'test@test.com',
      'year' => '1992',
      'month' => '10',
      'day' => '4',
      'email' => '',
      'be_empty' => '',
      'garbage' => uniqid()
    ];
  }

  public function t_garbage(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $input = [
      'my_field' => ' 11 ',
      'garbage1' => ' abcd',
      'garbage2' => 11,
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 11],
      'garbage' => ['garbage1' => ' abcd', 'garbage2' => 11],
      'filtered' => ['my_field' => true],
      'validated' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_only_filter(test $t)
  {
    $dict = fongo_read_dict(['data::dict/only_filter']);
    $input = [
      'my_field' => ' 10 '
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 10],
      'filtered' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_only_rule_valid(test $t)
  {
    $dict = fongo_read_dict(['data::dict/only_rule']);
    $input = [
      'my_field' => 11
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 11],
      'validated' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_filter_and_rule_valid(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $input = [
      'my_field' => ' 11 '
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 11],
      'filtered' => ['my_field' => true],
      'validated' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_filter_and_rule_not_valid(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $input = [
      'my_field' => ' -1 '
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => -1],
      'filtered' => ['my_field' => true],
      'errors' => ['my_field' => 'Invalid value'],
      'validated' => ['my_field' => true],
      'codes' => ['my_field' => 'rule/greater_than_10'],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_required_with_error(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $input = [];
    $t->wie = new fongo_result([
      'errors' => ['my_field' => 'Missing field'],
      'missed' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_required_with_custom_error(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->missing_message = 'my missing message';
    $input = [];
    $t->wie = new fongo_result([
      'errors' => ['my_field' => 'my missing message'],
      'missed' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_not_required(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->required = false;
    $dict['my_field']->missing_message = 'my missing message';
    $input = [];
    $t->wie = new fongo_result();
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_fill_with_value(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->fill_source = 'value';
    $dict['my_field']->fill_value = '100';
    $input = [];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 100],
      'filtered' => ['my_field' => true],
      'validated' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_fill_with_source_input(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule_my_other']);
    $dict['my_field']->fill_source = 'input';
    $dict['my_field']->fill_value = 'my_other_field';
    $input = [
      'my_other_field' => '200'
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 200, 'my_other_field' => 200],
      'filtered' => ['my_field' => true, 'my_other_field' => true],
      'validated' => ['my_field' => true, 'my_other_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_fill_with_source_input_and_invalid_fill_value(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule_my_other']);
    $dict['my_field']->fill_source = 'input';
    $dict['my_field']->fill_value = 'my_other_field_that_not_exists';
    $input = [
      'my_other_field' => '100'
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_other_field' => 100],
      'errors' => ['my_field' => 'Missing field'],
      'missed' => ['my_field' => true],
      'filtered' => ['my_other_field' => true],
      'validated' => ['my_other_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_fill_with_source_context(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->fill_source = 'context';
    $dict['my_field']->fill_value = 'fill_field';
    $input = [];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 100],
      'filtered' => ['my_field' => true],
      'validated' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_missed_fill_with_source_context_and_invalid_fill_value(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->fill_source = 'input';
    $dict['my_field']->fill_value = 'my_other_field_that_not_exists';
    $input = [];
    $t->wie = new fongo_result([
      'errors' => ['my_field' => 'Missing field'],
      'missed' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_compose_invalid(test $t)
  {
    $dict = fongo_read_dict(['data::dict/compose']);
    $input = [
    ];
    $t->wie = new fongo_result([
      'processed' => ['date' => '--'],
      'errors' => [
        'day' => 'Missing field',
        'year' => 'Missing field',
        'month' => 'Missing field',
        'date' => 'Is not a valid date'
      ],
      'missed' => [
        'day' => true,
        'year' => true,
        'month' => true
      ],
      'validated' => ['date' => true],
      'codes' => ['date' => 'invalid-date-error-code']
    ]);

    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_group_compose_valid(test $t)
  {
    $dict = fongo_read_dict(['data::dict/compose']);
    $input = [
      'year' => '2021',
      'month' => '  11  ',
      'day' => '22',
    ];
    $t->wie = new fongo_result([
      'processed' => [
        'year' => 2021,
        'month' => 11,
        'day' => 22,
        'date' => '2021-11-22',
      ],
      'filtered' => [
        'day' => true,
        'year' => true,
        'month' => true,
      ],
      'validated' => [
        'day' => true,
        'year' => true,
        'month' => true,
        'date' => true
      ],
    ]);

    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_empty_not_required_with_empty_function(test $t)
  {
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->required = false;
    $dict['my_field']->empty_allowed = true;
    $dict['my_field']->empty_function = [
      'function' => function ($value) { return '' === $value; }
    ];
    $input = [
      'my_field' => ''
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => ''],
      'empties' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_empty_not_required_without_empty_function(test $t)
  {
    /* empty condition is discarded because empty function is not defined */
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->required = false;
    $dict['my_field']->empty_allowed = true;
    $input = [
      'my_field' => ''
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => 0],
      'errors' => ['my_field' => 'Invalid value'],
      'filtered' => ['my_field' => true],
      'validated' => ['my_field' => true],
      'codes' => ['my_field' => 'rule/greater_than_10'],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_empty_required_with_empty_function(test $t)
  {
    /* empty condition is discarded because the field is required */
    $dict = fongo_read_dict(['data::dict/filter_and_rule']);
    $dict['my_field']->empty_allowed = true;
    $dict['my_field']->empty_function = [
      'function' => function ($value) { return '' === $value; }
    ];
    $input = [
      'my_field' => ''
    ];
    $t->wie = new fongo_result([
      'processed' => ['my_field' => ''],
      'empties' => ['my_field' => true],
    ]);
    $t->wig = fongo_process($dict, $input, $this->context);
    $t->pass_if($t->wie == $t->wig);
  }
}
