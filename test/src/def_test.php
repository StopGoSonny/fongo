<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/core/def.php';
  }


  public function t_php_callable(test $t)
  {
    $def = [
      'function' => 'is_int'
    ];
    $t->wie = ['is_int', [], ''];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_php_callable_args(test $t)
  {
    $def = [
      'function' => 'is_int',
      'args' => [1, 2]
    ];
    $t->wie = ['is_int', [1, 2], ''];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_callable(test $t)
  {
    $def = [
      'require' => FONGO_TEST_FUNC_DIR . '/rule/only_digits.php',
      'function' => 'fongo_test_rule_only_digits'
    ];
    $t->wie = ['fongo_test_rule_only_digits', [], ''];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_callable_args(test $t)
  {
    $def = [
      'require' => FONGO_TEST_FUNC_DIR . '/rule/only_digits.php',
      'function' => 'fongo_test_rule_only_digits',
      'args' => [1, 2]
    ];
    $t->wie = ['fongo_test_rule_only_digits', [1, 2], ''];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_require_fails(test $t)
  {
    $def = [
      'require' => uniqid(),
      'function' => uniqid()
    ];
    $t->wie = ['', [], sprintf('File %s not readable', $def['require'])];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_function_not_defined(test $t)
  {
    $def = [
      'functions' => uniqid()
    ];
    $t->wie = ['', [], 'function not defined'];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_function_not_callable(test $t)
  {
    $def = [
      'function' => uniqid()
    ];
    $t->wie = ['', [], sprintf('Function %s not callable', $def['function'])];
    $t->wig = fongo_def($def);
    $t->pass_if($t->wie === $t->wig);
  }

}
