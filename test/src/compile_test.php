<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/core/compile.php';
  }


  public function t_empty_path_only_name(test $t)
  {
    $t->wie = 'test.php';
    $t->wig = fongo_compile('test');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_empty_path_with_path_and_name(test $t)
  {
    $t->wie = 'test.php';
    $t->wig = fongo_compile('test::test');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_with_path_only_name(test $t)
  {
    fongo_path('test', '/mydir');
    $t->wie = '/mydir/test.php';
    $t->wig = fongo_compile('test');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_with_path_with_path_and_name(test $t)
  {
    fongo_path('test', '/mydir');
    $t->wie = '/mydir/test.php';
    $t->wig = fongo_compile('test::test');
    $t->pass_if($t->wie === $t->wig);
  }

}
