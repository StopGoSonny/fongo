<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/core/path.php';
  }


  public function t_all(test $t)
  {
    $t->wig = fongo_path();
    $t->pass_if(is_array($t->wig));
  }

  public function t_set(test $t)
  {
    $t->wie = null;
    $t->wig = fongo_path(__FUNCTION__, uniqid());
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_get(test $t)
  {
    $dir = uniqid();
    $t->wie = $dir;
    fongo_path(__FUNCTION__, $dir);
    $t->wig = fongo_path(__FUNCTION__);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_not_found(test $t)
  {
    $t->wie = '';
    $t->wig = fongo_path(uniqid());
    $t->pass_if($t->wie === $t->wig);
  }

}
