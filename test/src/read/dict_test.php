<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/read/dict.php';
    require_once FONGO_DIR . '/core/path.php';
    require_once FONGO_DIR . '/struct/term.php';
    fongo_path('test', __DIR__);
  }

  public function t_empty(test $t)
  {
    $t->wie = [
      'my_id' => new fongo_term()
    ];
    $t->wig = fongo_read_dict(['test::_dict_empty1']);
    $t->pass_if($t->wie == $t->wig);
  }

  public function t_array(test $t)
  {
    $t->wie = [
      'my_id' => new fongo_term([
        'type' => 'var',
        'compose' => '',
        'default' => null,
        'required' => true,
        'missing_message' => 'Missing field',
        'fill_source' => '',
        'fill_value' => null,
        'filters' => [],
        'rules' => [],
        'option' => ''
      ]),
      'test2' => new fongo_term([
        'type' => 'test',
        'compose' => 'test',
        'default' => 'test',
        'required' => false,
        'missing_message' => 'test',
        'fill_source' => 'test',
        'fill_value' => 'test',
        'filters' => ['test'],
        'rules' => ['test'],
        'option' => 'test',
      ])
    ];
    $t->wig = fongo_read_dict(['test::_dict_empty1', 'test::_dict_empty2']);
    $t->pass_if($t->wie == $t->wig);
  }
}
