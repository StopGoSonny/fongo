<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);
use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/read/rule.php';
    require_once FONGO_DIR . '/core/path.php';
    fongo_path('data', FONGO_TEST_DATA_DIR);
  }


  public function t_empty(test $t)
  {
    $t->wie = [
      'rule/is_bool' => ['function' => 'is_bool'],
      'rule/is_int' => ['function' => 'is_int']
    ];
    $t->wig = fongo_read_rule(['rule/is_bool', 'rule/is_int']);
    $t->pass_if($t->wie === $t->wig);
  }

}