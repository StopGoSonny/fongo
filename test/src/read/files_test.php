<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);
use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/read/files.php';
  }


  public function t_empty(test $t)
  {
    fongo_path('test', __DIR__);
    $t->wie = [
      'test1' => 1,
      'test2' => 2
    ];
    $t->wig = fongo_read_files(['test::_file1', 'test::_file2']);
    $t->pass_if($t->wie === $t->wig);
  }

}