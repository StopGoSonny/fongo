<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);
use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/read/filter.php';
    require_once FONGO_DIR . '/core/path.php';
    fongo_path('data', FONGO_TEST_DATA_DIR);
  }


  public function t_empty(test $t)
  {
    fongo_path('test', __DIR__);
    $t->wie = [
      'filter/trim' => ['function' => 'trim'],
      'filter/intval' => ['function' => 'intval']
    ];
    $t->wig = fongo_read_filter(['filter/trim', 'filter/intval']);
    $t->pass_if($t->wie === $t->wig);
  }

}