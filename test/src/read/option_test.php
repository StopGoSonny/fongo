<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);
use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/read/option.php';
    require_once FONGO_DIR . '/core/path.php';
    fongo_path('data', FONGO_TEST_DATA_DIR);
  }


  public function t_empty(test $t)
  {
    $t->wie = ['yes', 'no', 'y', 'n'];
    $t->wig = fongo_read_option(['option/yes_no', 'option/y_n']);
    $t->pass_if($t->wie === $t->wig);
  }

}