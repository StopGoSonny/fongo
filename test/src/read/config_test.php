<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);
use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/read/config.php';
  }


  public function t_not_found(test $t)
  {
    $t->wie = new exception();
    $t->wig = fongo_read_config(uniqid());
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_config_return_array(test $t)
  {
    $t->wie = ['test'];
    $t->wig = fongo_read_config(__DIR__ . '/_config_return_array.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_config_return_not_array(test $t)
  {
    $t->wie = [];
    $t->wig = fongo_read_config(__DIR__ . '/_config_return_not_array.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_config_declared_array(test $t)
  {
    $t->wie = ['test'];
    $t->wig = fongo_read_config(__DIR__ . '/_config_declared_array.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_config_declared_not_array(test $t)
  {
    $t->wie = [];
    $t->wig = fongo_read_config(__DIR__ . '/_config_declared_not_array.php');
    $t->pass_if($t->wie === $t->wig);
  }

}