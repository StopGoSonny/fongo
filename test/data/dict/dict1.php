<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$config = [
//  'field_with_only_filter' => [
//    'filters' => ['filter/trim'],
//  ],
  'field_with_only_rule' => [
    'rules' => ['rule/greater_than_10']
  ],
//  'dob' => [
//    'type' => 'group',
//    'group' => ['year', 'month', 'day'],
//    'compose' => '{year}-{month}-{day}'
//  ],
//  'year' => [
//    'filters' => ['intval'],
//    'rules' => ['is_int']
//  ],
//  'month' => [
//    'filters' => ['intval'],
//    'rules' => ['is_int']
//  ],
//  'day' => [
//    'filters' => ['intval'],
//    'rules' => ['is_int']
//  ],
//  'email' => [
//    'option' => ['yes_no'],
//    'filters' => ['cast_to_int'],
//  ],
//  'my_option' => [
//    'option' => ['yes_no'],
//    'filters' => ['cast_to_int'],
//  ],
];
