<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

return [
  'my_field' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/greater_than_10']
  ]
];

