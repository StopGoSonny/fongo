<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

return [
  'day' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/is_int']
  ],
  'year' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/is_int']
  ],
  'date' => [
    'type' => 'group',
    'compose' => '{year}-{month}-{day}',
    'rules' => ['rule/valid_date']
  ],
  'month' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/is_int', 'rule/greater_than_10']
  ],
];
