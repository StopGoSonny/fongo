<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$config = [
  'my_field' => [
    'rules' => ['rule/greater_than_10']
  ],
];
