<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

return [
  'dob_year' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/is_int']
  ],
  'dob_date' => [
    'type' => 'group',
    'compose' => '{dob_year}-{dob_month}-{dob_day}',
    'rules' => ['rule/valid_date']
  ],
  'dob_month' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/is_int']
  ],
  'dob_day' => [
    'filters' => ['filter/trim', 'filter/intval'],
    'rules' => ['rule/is_int', 'rule/greater_than_10']
  ],
];
