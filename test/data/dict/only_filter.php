<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$config = [
  'my_field' => [
    'filters' => ['filter/trim', 'filter/intval'],
  ],
];
