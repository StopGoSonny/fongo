<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

return [
  'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
  'function' => 'fongo_test_rule_greater_than',
  'args' => [10, false]
];

