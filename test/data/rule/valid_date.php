<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

return [
  'require' => FONGO_TEST_FUNC_DIR . '/rule/is_valid_date.php',
  'function' => 'fongo_test_rule_is_valid_date',
  'message' => 'Is not a valid date',
  'code' => 'invalid-date-error-code'
];

